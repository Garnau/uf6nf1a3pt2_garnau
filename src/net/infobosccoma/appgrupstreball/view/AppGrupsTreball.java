package net.infobosccoma.appgrupstreball.view;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import java.util.Iterator;
import java.util.Scanner;
import net.infobosccoma.appgrupstreball.model.Estudiant;
import net.infobosccoma.appgrupstreball.model.GrupTreball;

/**
 * Classe que conté el mètode principal de l'aplicació de gestió d'estudiants i
 * grups de treball.
 *
 * L'aplicació ha d gestionar, mitjançant una BDOO db4o, a quins grups de
 * treball pertanyen diferents estudiants dins una escola. Donat un grup de
 * treball, aquest pot tenir assignats diversos estudiants, però tot estudiant
 * només té un (però sempre un) únic grup de treball. L’aplicació ha de poder
 * fer el següent: - Donar d’alta un nou estudiant. A l’hora d’assignar-li un
 * grup, si el nom indicat no existeix, es crea un nou grup. Si existeix, a
 * l’estudiant se li assigna aquell grup. - Reassignar un estudiant a un altre
 * grup de treball. Aquest grup ja ha d’existir. Si, en fer-ho, el grup antic
 * queda sense membres, cal esborrar-lo de la BDOO. - Llistar tots els grups
 * existents. - Llistar tots els estudiants (i a quin grup pertanyen).
 *
 * Es considera que els noms dels grups i dels estudiants són únics al sistema.
 * No hi pot haver noms repetits. En base a la descripció, també cal remarcar
 * que l’única manera de crear grups nous és afegint-hi nous estudiants.
 *
 * @author GARNAU
 */
public class AppGrupsTreball {

    static final Scanner reader = new Scanner(System.in);

    static Integer select = 0;

    public static void main(String[] args) {

        ObjectContainer db = Db4oEmbedded.openFile("GrupsTreballBD.db4o");

        System.out.println("ESCULL UNA OPCIÓ");
        System.out.println("================\n");

        System.out.println("1 - Donar d'alta un estudiant");
        System.out.println("2 - Reassignar un estudiant");
        System.out.println("3 - Llistar tots els grups");
        System.out.println("4 - Llistar tots els estudiants");
        System.out.println("5 - Sortir");

        do {
            System.out.print("\nEscull una opció: ");
            select = reader.nextInt();
            System.out.println("");

            switch (select) {
                case 1:
                    crearEstudiant(db);
                    break;

                case 2:
                    reassignarEstudiant(db);
                    break;

                case 3:
                    llistarGrups(db);
                    break;

                case 4:
                    llistarEstudiants(db);
                    break;

                case 5:
                    db.close();
                    System.out.println("FINS AVIAT!");
                    break;
            }
        } while (select != 5);
    }

    //CREEM ESTUDIANTS
    public static void crearEstudiant(ObjectContainer db) {

        try {

            System.out.print("Nom estudiant: ");
            String nom = reader.next();

            System.out.print("Grup de treball: ");
            String grup = reader.next();

            System.out.print("Tema del grup: ");
            String tema = reader.next();

            GrupTreball grupTreball = new GrupTreball(grup, null);

            //COMPROVEM QUE ENS OMPLEN TOTS ELS CAMPS
            if (nom.equals("") || grup.equals("") || tema.equals("")) {
                System.out.println("Tots els camps són obligatoris");
            } else {
                
                // COMPROVEM SI EL GRUP DE TREBALL ESCOLLIT EXISTEIX O NO
                grupTreball = comprovarGrupTreball(db, grupTreball, grup, tema);

                // AFEGIM EL NOU ESTUDIANT 
                Estudiant estudiant = new Estudiant(nom, grupTreball);
                System.out.println("Estudiant " + estudiant.getNom() + " creat.");
                db.store(estudiant);
            }

        } catch (Exception e) {
            System.out.println("Error al afegir l'usuari");
        }

    }

    // MOSTREM EL QUE ENS PASSIN COM A PARÀMETRE
    static void mostrar(ObjectSet result) {

        Iterator it = result.iterator();

        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }

    //MOSTREM TOTS ELS GRUPS
    private static void llistarGrups(ObjectContainer db) {

        ObjectSet result = db.queryByExample(GrupTreball.class);
        System.out.println("Nº de grups: " + result.size() + "\n");

        mostrar(result);

    }

    //LLISTEM TOTS ELS ESTUDIANTS
    private static void llistarEstudiants(ObjectContainer db) {

        // MOSTREM TOTS ELS ESTUDIANTS
        ObjectSet result = db.queryByExample(Estudiant.class);
        System.out.println("Nº d'estudiants: " + result.size() + "\n");

        mostrar(result);

    }

    // REASSIGNEM EL GRUP D'UN ESTUDIANT
    private static void reassignarEstudiant(ObjectContainer db) {

        System.out.print("Nom de l'estudiant: ");
        String nom = reader.next();

        System.out.print("Nom del grup nou: ");
        String grupNou = reader.next();
        
        
        // ERROR, NO EM FUNCIONA
        Estudiant estudiant = new Estudiant(nom, null);
        
        GrupTreball grupTreball = new GrupTreball(grupNou, null);
            
        if (nom.equals("") || grupNou.equals("")) {
            System.out.println("Tots els camps són obligatoris");
        } else {
            
            //OBTENIM EL GRUP DE TREBALL ESCOLLIT
            grupTreball = obtenirGrup(db, grupTreball);
            
            //OBTENIM L'ESTUDIANT
            estudiant = obtenirEstudiant(db, estudiant);
            
            //ASSIGNEM EL NOU GRUP
            estudiant.reassignaGrup(grupTreball);  
        
            //GUARDEM ELS CANVIS
            db.store(estudiant);
        
            System.out.println("Grup actualitzat correctament");
        }
    }

    // COMPROVEM SI EL GRUP DE TREBALL EXISTEIX O NO
    private static GrupTreball comprovarGrupTreball(ObjectContainer db, GrupTreball grupTreball, String grup, String tema) {
        ObjectSet<GrupTreball> result = db.queryByExample(grupTreball);
        
        // SI TROBA ALGO VOLDRÀ DIR QUE EXISTEIX
        if (result.size() > 0) {
            System.out.println("El grup ja existeix");
            
            // AGAFEM EL GRUP I EL GUARDEM EN LA VARIABLE grupTreball
            grupTreball = result.next();
            
            // INCREMENTEM EL Nº D'ESTUDIANTS AL GRUP
            grupTreball.sumaEstudiant();
            System.out.println("Nº estudiants al grup " + grup + ": " + grupTreball.getNumEstudiants());
            db.store(grupTreball);
            return grupTreball;

        } else {
            System.out.println("Aquest grup no existeix, creant-lo...");
            
            // CREEM UN GRUP NOU
            GrupTreball nouGrupTreball = new GrupTreball(grup, tema);
            db.store(nouGrupTreball);
            return nouGrupTreball;

        }

    }

    // OBTENIM L'ESTUDIANT PASSAT PER PARÀMETRE
    private static Estudiant obtenirEstudiant(ObjectContainer db, Estudiant estudiant) {
        ObjectSet<Estudiant> result = db.queryByExample(estudiant);
        System.out.println(result.next().toString());
        return result.next();
    }

    // OBTENIM L'ESTUDIANT PASSAT PER PARÀMETRE
    private static GrupTreball obtenirGrup(ObjectContainer db, GrupTreball grupTreball) {
        ObjectSet<GrupTreball> result = db.queryByExample(grupTreball);
        grupTreball = result.next();
        return grupTreball;
    }
}
